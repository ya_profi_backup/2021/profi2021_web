import json
import os
import subprocess
import requests
import base64
import time
import git

from logger import *
# from metrics_graber import *


class workspace_manager:
    def __init__(self, workspace):
        self.workspace_path = os.path.expanduser(workspace)

    def __enter__(self):
        self.saved_path = os.getcwd()
        os.chdir(self.workspace_path)


    def __exit__(self, etype, value, traceback):
        os.chdir(self.saved_path)

class Referee:
    def __init__(self, config, docker_work_path):
        self.repos_dict = {}
        self.id_counter=0
        self.config_json = json.load(open(config))["referee_config"]
        self.base_ws_urls = self.config_json["base_ws_urls"]
        self.test_repo_name = self.config_json["test_repo_name"]
        self.logger = Logger()
        os.chdir(docker_work_path)
        print("[Status] Change current repo on: ", os.getcwd())
        self.referee_init()

    def __del__(self):
        output = subprocess.run("docker stop profi2021", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(output.stdout.decode("utf-8"))

    def test_solution(self, url):
        if url not in self.repos_dict.keys():
            g = git.cmd.Git()
            resp = g.ls_remote( url)
            if 'HEAD' in resp:
                self.repos_dict[url]={"id": self.id_counter}
                self.id_counter+=1
                id_ws_path = "workspace/ws"+str(self.repos_dict[url]["id"])
                os.makedirs(id_ws_path)
                os.makedirs(id_ws_path+"/src")

                # load new solution
                with workspace_manager(id_ws_path+"/src"):
                    self.logger.set_path("../")
                    self.logger.savelog(LOAD_LOG, self.load_solution(url))
            else:
                print("[Error] Given bad addres!")
        else:
            id_ws_path = "workspace/ws"+str(self.repos_dict[url]["id"])
            with workspace_manager(id_ws_path+"/src"):
                self.logger.set_path("../")
                log = self.update_solution(url)
                self.logger.savelog(LOAD_LOG, log)

        # build solution
        id_ws_path = "workspace/ws"+str(self.repos_dict[url]["id"])
        self.logger.set_path("./")
        with workspace_manager(id_ws_path):
            log = self.build_solution(id_ws_path)
            self.logger.savelog(BUILD_LOG, log)

    def referee_init(self):
        self.load_base_ws()
        self.build_base_ws()

    def load_base_ws(self):
        print("[Status] Preparing base repositories ...")
        newfolder = "workspace/base_ws"
        print("[Status] Given next base repos: ", self.base_ws_urls)
        if not os.path.exists(newfolder):
            os.makedirs(newfolder)
            os.makedirs(newfolder+"/src")
            with workspace_manager(newfolder+"/src"):
                print("[Status] Clone base repos")
                for url in self.base_ws_urls:
                    repo_name = url.split("/")[-1]
                    repo_name = repo_name.replace('.git', '')
                    output = subprocess.run("git clone " + url+"; cd " + repo_name + "; git submodule update --init --recursive"+ "; cd ..", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
                    print(output.stderr.decode('utf-8'))

    def build_base_ws(self):
        output = subprocess.run("bash docker/run_docker_still.sh", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        build_ws_log = output.stdout.decode("utf-8")
        print(output.stdout.decode("utf-8"))
        print("[Status] Start building base repositories")
        output = subprocess.run("docker exec -t profi2021 bash -c \"source /opt/ros/melodic/setup.bash; cd workspace/base_ws; catkin build; source devel/setup.bash;\"", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        build_ws_log += output.stdout.decode("utf-8")
        print(output.stdout.decode("utf-8"))
        print("[Status] Base repos are ready")


    def load_solution(self,url):
        load_log = ""
        output = subprocess.run("git clone " + url, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        load_log += output.stdout.decode("utf-8")+"\n"
        print(load_log)
        return load_log

    def update_solution(self,url):
        load_log = ""
        output = subprocess.run("cd "+ self.test_repo_name+"; git pull origin master;", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
        load_log += output.stdout.decode("utf-8")+"\n"
        print(load_log)
        return load_log

    def run_solution(self):
        pass

    def build_solution(self, path):
        output = subprocess.run("docker exec -t profi2021 bash -c \"source /opt/ros/melodic/setup.bash; cd workspace/; source base_ws/devel/setup.bash; cd ../"+ path + "; catkin build; source devel/setup.bash; cd ..\"", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(output.stdout.decode("utf-8"))
        build_log = output.stderr.decode("utf-8")+"\n"
        return build_log
