from datetime import datetime

BUILD_LOG ='build_log'
LOAD_LOG = 'load_log'
RUN_LOG = 'run_log'

class Logger():
    def __init__(self, path=""):
        self.set_path(path)

    def set_path(self,path):
        self.path = path

    def savelog(self, type, str):
            f = open(self.path + type+ datetime.now().strftime('%d-%H-%M-%S') + ".log", "w")
            f.write(str)
            f.close()
