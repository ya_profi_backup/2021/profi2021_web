#!/usr/bin/env bash

bash docker/run_docker_still.sh
echo "Into docker\n"
docker exec -it profi2021 bash -c "source /opt/ros/melodic/setup.bash; cd workspace/base_ws; catkin build"
