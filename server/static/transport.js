var host = window.location.host;
var ws = new WebSocket('ws://'+host+'/ws');
ws.onmessage = function(msg) {
    data=JSON.parse(msg.data);
    console.log(data);
    parse_message(data);
}

function parse_message(message) {
    if ("client_state" in message) {
        if (message["client_state"] === "ready") {
              ready_state();
        }
        else if (message["client_state"] === "in_queue") {
            if (message["queue_status"]["pos"] === 0){
                testing_state();
            } else {
                in_queue_state(message["queue_status"]);
            }
        }
        else if (message["client_state"] === "testing") {
        testing_state();
        }
        else if (message["client_state"] === "await") {
        await_state(message["await_status"]);
        }
        else if (message["client_state"] === "unauthorized") {
        unauthorized_state();
        }
        else {
        unauthorized_state();
        }
    }

    if ("results" in message) {
        if (message["results"].length === 0) {
            document.getElementById("results").style.visibility = "hidden";
        } else {
            generate_result_table(message["results"]);
            document.getElementById("results").style.visibility = "visible";
        }
    }
}

function on_user_id_change() {
    var user_id = document.getElementById("user_id").value;
    send_user_id_to_server(user_id)
}

function send_user_id_to_server(user_id) {
  var message = {"user_id": user_id}
  ws.send(JSON.stringify(message));
}

function unauthorized_state() {
    document.getElementById("unauthorized_ctr").style.display = "block";
    document.getElementById("ready_ctr").style.display = "none";
    document.getElementById("in_queue_ctr").style.display = "none";
    document.getElementById("testing_ctr").style.display = "none";
    document.getElementById("await_ctr").style.display = "none";
    document.getElementById("results").style.visibility = "hidden";
}

function ready_state() {
    document.getElementById("unauthorized_ctr").style.display = "none";
    document.getElementById("ready_ctr").style.display = "block";
    document.getElementById("in_queue_ctr").style.display = "none";
    document.getElementById("testing_ctr").style.display = "none";
    document.getElementById("await_ctr").style.display = "none";
    // document.getElementById("results").style.visibility = "visible";
}

function in_queue_state(queue_status) {
    document.getElementById("in_queue_info").innerHTML = "Место в очереди: " + queue_status["pos"]; // + "<br>Примерное время ожидания: " + queue_status["atime"];
    document.getElementById("unauthorized_ctr").style.display = "none";
    document.getElementById("ready_ctr").style.display = "none";
    document.getElementById("in_queue_ctr").style.display = "block";
    document.getElementById("testing_ctr").style.display = "none";
    document.getElementById("await_ctr").style.display = "none";
    // document.getElementById("results").style.visibility = "visible";
}

function testing_state() {
    document.getElementById("unauthorized_ctr").style.display = "none";
    document.getElementById("ready_ctr").style.display = "none";
    document.getElementById("in_queue_ctr").style.display = "none";
    document.getElementById("testing_ctr").style.display = "block";
    document.getElementById("await_ctr").style.display = "none";
    // document.getElementById("results").style.visibility = "visible";
}

function await_state(await_status) {
    var info = "Отправить решение можно будет через: ";

    var amin = await_status["amin"] % 60;
    var ahrs = Math.floor(await_status["amin"] / 60);

    if (ahrs > 0) {
        info = info + ahrs + " ч. ";
    }
    info = info + amin + " м.";

    document.getElementById("await_info").innerHTML = info;
    document.getElementById("unauthorized_ctr").style.display = "none";
    document.getElementById("ready_ctr").style.display = "none";
    document.getElementById("in_queue_ctr").style.display = "none";
    document.getElementById("testing_ctr").style.display = "none";
    document.getElementById("await_ctr").style.display = "block";
    // document.getElementById("results").style.visibility = "visible";
}

function start_test() {
    var message = {"request": "start_test"}
    ws.send(JSON.stringify(message));
}

function cancel_test() {
    var message = {"request": "cancel_test"}
    ws.send(JSON.stringify(message));
}


function generate_result_table(results) {

    var result_table_ctr = document.getElementById("result_table");

    while(result_table_ctr.firstChild){
        result_table_ctr.removeChild(result_table_ctr.firstChild);
    }

    var table = document.createElement('TABLE');
    // table.classList.add('MyClass');
    // table.border = '1';

    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);

    // HEADER
    var tr = document.createElement('TR');
    tr.classList.add('hrow');
    tableBody.appendChild(tr);

    var result_time_td = document.createElement('TD');
    var result_data_td = document.createElement('TD');
    var result_td = document.createElement('TD');

    result_time_td.classList.add('tcell');
    result_data_td.classList.add('tcell');
    result_td.classList.add('tcell');

    result_time_td.appendChild(document.createTextNode("ВРЕМЯ"));
    result_data_td.appendChild(document.createTextNode("ДАТА"));
    result_td.appendChild(document.createTextNode("РЕЗУЛЬТАТ"));

    tr.appendChild(result_time_td);
    tr.appendChild(result_data_td);
    tr.appendChild(result_td);

    var i = 1;
    for (result of results) {
        var tr = document.createElement('TR');
        if ((i % 2) > 0 ) {
            // console.log(1);
            tr.classList.add('trow1');
        } else {
            // console.log(2);
            tr.classList.add('trow2');
        }
        tableBody.appendChild(tr);
        i = i + 1;

        var result_time_td = document.createElement('TD');
        var result_data_td = document.createElement('TD');
        var result_td = document.createElement('TD');

        result_time_td.classList.add('tcell');
        result_data_td.classList.add('tcell');

        result_time_td.appendChild(document.createTextNode(result["time"]));

        result_data_td.appendChild(document.createTextNode(result["data"]));

        if (result["status"] === "error" ) {
            result_td.classList.add('tercell');
            var result = "ОШИБКА"
        } else if (result["status"] === "error_test" ) {
            result_td.classList.add('tercell');
            var result = "ОШИБКА ТЕСТИРОВАНИЯ"
        } else if (result["status"] === "error_build" ) {
            result_td.classList.add('tercell');
            var result = "ОШИБКА СБОРКИ"
        }else {
            var result = result["score"]
            result_td.classList.add('tcell');
        }

        result_td.appendChild(document.createTextNode(result));

        tr.appendChild(result_time_td);
        tr.appendChild(result_data_td);
        tr.appendChild(result_td);

    }

    result_table_ctr.appendChild(table);
}
