import json
import sys, os

fpath = os.path.dirname(sys.argv[0])

PARTICIPANTS_DB = fpath + "/data/participants.json"

participants_db = {}


class ParticipantStatus():
    READY   = "ready"
    INQUEUE = "in_queue"
    TESTING = "testing"
    AWAIT   = "await"


def new_participant(id, repo_url, name = None):

    id = str(id)

    if repo_url in participants_db:
        return False

    for _, participant_data in participants_db.items():
        if (participant_data["id"] == id):
            return False

    participants_db.update({repo_url: {"id":       id,
                                       "repo_url": repo_url,
                                       "name":     name,
                                       "status":   ParticipantStatus.READY,
                                       "cur_test":  None,
                                       "results":  []}})
    return True


def update_participants_db():

    global participants_db

    try:
        with open(PARTICIPANTS_DB, mode='w') as db:
            db.write(json.dumps(participants_db, sort_keys=True, indent=2))
            db.flush()
            return True
    except:
        print("Не могу сохранить в базу данных " + PARTICIPANTS_DB)
        return False


def load_participants_from_db():

    global participants_db

    try:
        with open(PARTICIPANTS_DB, mode='r') as db:
            content = db.read()
            participants_db = json.loads(content)
            db.close()
            return True
    except:
        print("Не могу открыть базу данных " + PARTICIPANTS_DB)
        return False


def main():

    global participants_db

    if not load_participants_from_db():
        exit(1)

    while True:
        print("ID Участника: ")
        id = input()

        print("Репозиторий Участника: ")
        repo_url = input()

        print("ФИО Участника: ")
        name = input()

        if (id == "" or repo_url == ""):
            continue


        if not new_participant(id, repo_url, name):
            print("Участник с такими данными уже существует!")
            continue

        if not update_participants_db():
            exit(1)

        print("Участник добавлен\n")


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        exit(0)
