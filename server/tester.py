import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import asyncio
import aiofiles as aiof
import time
import datetime
from time import gmtime, strftime
import os
import os.path
import sys
import json

from tornado.options import define, options

fpath = os.path.dirname(sys.argv[0])

AVERAGE_TEST_TIME = 1       # in minutes
TESTING_TIMEOUT = 3         #

PARTICIPANTS_DB = fpath + "/data/participants.json"
QUEUE_BK =        fpath + "/data/testing_bk.json"

ws_clients = {}
participants_db = {}
tests_queue = []


class ParticipantStatus():
    READY   = "ready"
    INQUEUE = "in_queue"
    TESTING = "testing"
    AWAIT   = "await"


class ResultStatus():
    PENDING     = "pending"       # in queue
    ACTIVE      = "active"        # testing right now
    ERROR       = "error"         # error
    ERROR_TEST  = "error_test"    # error during testing
    ERROR_BUILD = "error_build"   # error during building
    PASSED      = "passed"        # test passed successfully


def new_participant(id, repo_url, name = None):

    id = str(id)

    if repo_url in participants_db:
        return False

    for _, participant_data in participants_db.items():
        if (participant_data["id"] == id):
            return False

    participants_db.update({repo_url: {"id":       id,
                                       "repo_url": repo_url,
                                       "name":     name,
                                       "status":   ParticipantStatus.READY,
                                       "cur_test":  None,
                                       "results":  []}})
    return True


async def update_participants_db():

    global participants_db

    async with aiof.open(PARTICIPANTS_DB, mode='w') as db:
        await db.write(json.dumps(participants_db, sort_keys=True, indent=2))
        await db.flush()
        # await db.close()


async def update_queue_bk():

    global tests_queue

    async with aiof.open(QUEUE_BK, mode='w') as db:
        await db.write(json.dumps(tests_queue, indent=2))
        await db.flush()
        # await db.close()


def load_participants_from_db():

    global participants_db

    try:
        with open(PARTICIPANTS_DB, mode='r') as db:
            content = db.read()
            participants_db = json.loads(content)
            db.close()
            return True
    except:
        logging.info("Can't load participants from " + PARTICIPANTS_DB)
        return False


def load_queue_bk():

    global tests_queue

    try:
        with open(QUEUE_BK, mode='r') as bk:
            content = bk.read()
            tests_queue = json.loads(content)
            bk.close()
            return True
    except:
        logging.info("Can't load testing queue backup from " + QUEUE_BK)
        return False


def reset_participant_statuses():
    for _, participant_data in participants_db.items():
        participant_data["status"] = ParticipantStatus.READY
        participant_data["cur_test"] = None


def get_await_time(repo_url):

    if repo_url in participants_db:

        if participants_db[repo_url]["results"]:

            last_test_time = participants_db[repo_url]["results"][0]["time"]
            last_test_data = participants_db[repo_url]["results"][0]["data"]

            test_date_time = datetime.datetime.strptime(last_test_data + " " + last_test_time, "%d.%m.%Y %H:%M")
            cur_date_time  = datetime.datetime.now()

            test_time = time.mktime(test_date_time.timetuple())
            cur_time  = time.mktime(cur_date_time.timetuple())

            delta_mins = TESTING_TIMEOUT - (cur_time - test_time) / 60.

            return int(delta_mins)

    return 0


def get_test_queue_pos(repo_url):

    queue_pos = None

    for i in range(len(tests_queue)):
        if repo_url in tests_queue[i]:
            queue_pos = i

    return queue_pos


### TESTER SIDE STATUS CHANGE

def get_next_repo_for_test():
    for i in range(len(tests_queue)):
        # logging.info(tests_queue[i])
        repo_url = next(iter(tests_queue[i]))
        if tests_queue[i][repo_url]["status"] == ResultStatus.PENDING:
            return {"repo_url" : repo_url, "test_info": tests_queue[i][repo_url]}
    return None


def change_test_status_to_active(repo_url):
    queue_pos = get_test_queue_pos(repo_url)
    if queue_pos is not None:
        tests_queue[queue_pos][repo_url]["status"] = ResultStatus.ACTIVE
        participants_db[repo_url]["status"] = ParticipantStatus.TESTING

        for connection in ws_clients:
            if ws_clients[connection]:
                if ws_clients[connection]["repo_url"] == repo_url:
                    connection.write_message({"client_state": "testing"})


async def set_test_result(repo_url, score, error = None):

    if await remove_test_from_queue(repo_url):

        participants_db[repo_url]["results"].insert(0, dict(participants_db[repo_url]["cur_test"]))
        participants_db[repo_url]["results"][0]["score"] = score

        if error:
            participants_db[repo_url]["results"][0]["status"] = error
        else:
            participants_db[repo_url]["results"][0]["status"] = ResultStatus.PASSED

        participants_db[repo_url]["cur_test"] = None

        amin = get_await_time(repo_url)
        if amin < 1:
            participants_db[repo_url]["status"] = ParticipantStatus.READY
        else:
            participants_db[repo_url]["status"] = ParticipantStatus.AWAIT

        await update_participants_db()

        for connection in ws_clients:
            if ws_clients[connection]:
                if ws_clients[connection]["repo_url"] == repo_url:

                    if amin < 1:
                        connection.write_message({"client_state": "ready",
                                                  "results":      participants_db[repo_url]["results"]})

                    else:
                        connection.write_message({"client_state": "await",
                                                  "await_status": {"amin": amin,},
                                                  "results":      participants_db[repo_url]["results"]})

        for connection in ws_clients:
            # update queue info for all inqueue clients
            if ws_clients[connection]:
                if ws_clients[connection]["status"] == ParticipantStatus.INQUEUE:
                    queue_pos = get_test_queue_pos(ws_clients[connection]["repo_url"])
                    if queue_pos is not None:
                        wait_time = queue_pos * AVERAGE_TEST_TIME
                        connection.write_message({"client_state": "in_queue",
                                                  "queue_status": {"pos": queue_pos, "wait_time": wait_time}})


### SERVER SIDE STATUS CHANGE

async def await_monitor():

    while True:
        # logging.info("Await_monitor check")

        for repo_url, participant_data in participants_db.items():

            if participant_data["status"] == ParticipantStatus.AWAIT:

                amin = get_await_time(repo_url)
                # logging.info("amin: " + str(amin))
                if amin < 1:
                    participant_data["status"] = ParticipantStatus.READY

                await update_participants_db()

                for connection in ws_clients:
                    if ws_clients[connection]:
                        if ws_clients[connection]["repo_url"] == repo_url:

                            if amin < 1:
                                connection.write_message({"client_state": "ready"})
                            else:
                                connection.write_message({"client_state": "await",
                                                          "await_status": {"amin": amin,}})

        await asyncio.sleep(4)


### CLIENT SIDE STATUS CHANGE

async def start_new_test(repo_url):

    queue_pos = None
    if repo_url in participants_db:

        if (get_test_queue_pos(repo_url) is not None):
            return None


        participants_db[repo_url]["status"] = ParticipantStatus.INQUEUE
        participants_db[repo_url]["cur_test"] = {"data":   strftime("%d.%m.%Y", time.localtime()),
                                                 "time":   strftime("%H:%M",    time.localtime()),
                                                 "status": ResultStatus.PENDING,
                                                 "score":  0}

        await update_participants_db()

        tests_queue.append({repo_url: participants_db[repo_url]["cur_test"]})
        await update_queue_bk()

        queue_pos = len(tests_queue) - 1

    return queue_pos


async def remove_test_from_queue(repo_url):

    removed = None
    queue_pos = get_test_queue_pos(repo_url)

    if queue_pos is not None:
        removed = tests_queue.pop(queue_pos)
        await update_queue_bk()

    return removed


async def cancel_test(repo_url):

    if (repo_url in participants_db):
        if (participants_db[repo_url]["status"] == ParticipantStatus.INQUEUE):
            if await remove_test_from_queue(repo_url):
                participants_db[repo_url]["cur_test"] = None
                participants_db[repo_url]["status"] = ParticipantStatus.READY
                await update_participants_db()
                return True

    return False

### ----

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/", IndexHandler), (r"/ws", WebSocketHandler)]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        super().__init__(handlers, **settings)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class WebSocketHandler(tornado.websocket.WebSocketHandler):

    def open(self, *args):
        logging.info("New connection from: " + str(self.request.remote_ip) )
        if self not in ws_clients:
            ws_clients.update({self: None})     # unauthorized client


    async def on_message(self, raw_message):
        # logging.info(raw_message)
        message = tornado.escape.json_decode(raw_message)

        if "user_id" in message:

            for repo_url, participant_data in participants_db.items():
                if (participant_data["id"] == message["user_id"]):
                    logging.info(message)

                    ws_clients.update({self: participant_data})      # client authorization

                    answer = {"client_state": participant_data["status"],
                              "results":      participant_data["results"]}

                    if (participant_data["status"] == ParticipantStatus.INQUEUE):
                        answer.update({"queue_status": {"pos": get_test_queue_pos(participant_data["repo_url"])}})

                    if (participant_data["status"] == ParticipantStatus.AWAIT):
                        amin = get_await_time(participant_data["repo_url"])
                        answer.update({"await_status": {"amin": amin}})

                    self.write_message(answer)

            # logging.info(tests_queue)

        if "request" in message:

            if ws_clients[self]:                    # client authorization check

                if message["request"] == "start_test":
                    if ws_clients[self]["status"] == ParticipantStatus.READY:
                        queue_pos = await start_new_test(ws_clients[self]["repo_url"])
                        if queue_pos is not None:
                            wait_time = queue_pos * AVERAGE_TEST_TIME

                            for connection in ws_clients:
                                if ws_clients[connection]:
                                    if ws_clients[connection]["repo_url"] == ws_clients[self]["repo_url"]:
                                        connection.write_message({"client_state": "in_queue",
                                                                  "queue_status": {"pos": queue_pos, "wait_time": wait_time}})

                if message["request"] == "cancel_test":
                    if ws_clients[self]["status"] == ParticipantStatus.INQUEUE:
                        if (await cancel_test(ws_clients[self]["repo_url"])):

                            for connection in ws_clients:
                                if ws_clients[connection]:
                                    if ws_clients[connection]["repo_url"] == ws_clients[self]["repo_url"]:
                                        connection.write_message({"client_state": "ready"})

                                    if ws_clients[connection]["status"] == ParticipantStatus.INQUEUE:
                                        queue_pos = get_test_queue_pos(ws_clients[connection]["repo_url"])
                                        if queue_pos is not None:
                                            wait_time = queue_pos * AVERAGE_TEST_TIME
                                            connection.write_message({"client_state": "in_queue",
                                                                      "queue_status": {"pos": queue_pos, "wait_time": wait_time}})


            # logging.info(tests_queue)


    def on_close(self):
        del ws_clients[self]
        logging.info("Connection " + str(self.request.remote_ip) + " closed.")


async def fake_tester():
    # logging.info("Fake_tester check")

    while True:

        test = get_next_repo_for_test()

        if test:
            # logging.info("Fake_tester got: " + str(test))
            change_test_status_to_active(test["repo_url"])
            await asyncio.sleep(AVERAGE_TEST_TIME * 5)
            score = int(strftime("%M", time.localtime()))
            if (score % 5 == 0):
                error = ResultStatus.ERROR_TEST
            elif (score % 6 == 0):
                error = ResultStatus.ERROR_BUILD
            else:
                error = None

            await set_test_result(test["repo_url"], score, error)

        else:
            # logging.info("Fake_tester check")
            await asyncio.sleep(5)


def main():
    tornado.options.parse_command_line()

    load_participants_from_db()

    if not load_queue_bk():
        reset_participant_statuses()

    # new_participant("4Fd3G4dd38", "gl1")
    # new_participant( 7454823947,  "gl2")
    # new_participant( 5829392834,  "gl3", "Kolya Dema")
    # new_participant( 2353245655,  "gl4", "Kirill Artemov")
    # new_participant( 4563234333,  "gl5", "Vlad Antipov")

    define("port", default=4088, help="run on the given port", type=int)

    app = Application()
    app.listen(options.port)

    # tornado.ioloop.IOLoop.current().spawn_callback(update_participants_db)

    tornado.ioloop.IOLoop.current().spawn_callback(fake_tester)
    tornado.ioloop.IOLoop.current().spawn_callback(await_monitor)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()



if __name__ == "__main__":
    main()
